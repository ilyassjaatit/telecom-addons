import csv
import base64
from io import StringIO

from odoo import models, fields
from odoo.tools.translate import _

IMPORT_TYPE_CATEGORY = 'categories'
IMPORT_TYPE_PRODUCT = 'products'
IMPORT_TYPES = [
    (IMPORT_TYPE_CATEGORY, _('Import Categories')),
    (IMPORT_TYPE_PRODUCT, _('Import Products'))
]


class ImportWizard(models.TransientModel):
    _name = 'telecom_consumptions.import_wizard'
    _description = 'Import Categories and Products Wizard'

    import_type = fields.Selection(IMPORT_TYPES, string='Import Type', required=True)
    data_file = fields.Binary(string='Data File', required=True, help="Allowed formats: csv")
    data_filename = fields.Char(string='Filename')

    def _process_csv_file(self, file_bins, delimiter=','):
        """
        Process a CSV file and return a list of dictionaries.

        Args:
            file_bins (bytes): Binary data of the CSV file.
            delimiter (str, optional): Delimiter used in the CSV file. Defaults to ','.

        Returns:
            list: A list of dictionaries representing the rows in the CSV file.

        """

        file_content = base64.b64decode(file_bins)

        with StringIO(file_content.decode('utf-8')) as file_obj:
            csv_reader = csv.DictReader(file_obj, delimiter=delimiter)
            rows = list(csv_reader)

        return rows

    def import_data(self):
        """

        """
        # TODO Give more information to the user about the errors in the file
        data_items = self._process_csv_file(self.data_file)
        if self.import_type == IMPORT_TYPE_CATEGORY:
            self.env['product.category'].create_category_telecom_by_dict(data_items)
        elif self.import_type == IMPORT_TYPE_PRODUCT:
            self.env['product.product'].create_product_telecom_by_dict(data_items)
