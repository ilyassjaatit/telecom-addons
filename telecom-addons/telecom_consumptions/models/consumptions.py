from odoo import fields, models


class Consumptions(models.Model):
    _name = 'telecom_consumptions.consumptions'
    _description = 'Telecom Consumptions'

    timestamp = fields.Datetime(string='Timestamp')
    product_id = fields.Many2one('product.product', string='Product')
    quantity = fields.Float(string='Quantity')
