from odoo import fields, models
from odoo.tools.translate import _


class ProductCategory(models.Model):
    _inherit = 'product.category'
    code = fields.Char(string='Code')

    _sql_constraints = [
        ('unique_category_code', 'unique(code)', _('Category can only have one unique code!'))
    ]

    def create_category_telecom_by_dict(self, items):
        """
            Create categories Odoo by dict .

            Args:
                items (list): A list containing the category data to be created.

            Returns:

        """
        # TODO improve the data import process, Add error handling and list duplicate values and errors
        for item in items:
            record = self.search([('code', '=', item['code'])])
            if not record:
                self.create({
                    'name': item['name'],
                    'code': item['code'],
                })
