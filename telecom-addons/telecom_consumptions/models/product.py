from odoo import fields, models


class Product(models.Model):
    _inherit = 'product.product'

    def create_product_telecom_by_dict(self, items):
        """
        Create products based on a dictionary of items.

        Args:
            items (dict): A dictionary containing the items to create products.

        Returns:
            None
        """
        # TODO improve the data import process, Add error handling and list duplicate values and errors
        for item in items:
            record_cat = self.env['product.category'].search([('code', '=', item['category'])], limit=1)
            if not record_cat:
                continue
            record_product = self.search([
                ('default_code', '=', item['code']),
            ])

            if not record_product:
                self.create({
                    'name': item['name'],
                    'default_code': item['code'],
                    'categ_id': record_cat[0].id,
                })
