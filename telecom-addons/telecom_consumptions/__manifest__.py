{
    'name': 'Telecom Consumptions',
    'version': '0.0.0.0.2',
    'description': "",
    'category': 'Telecom',
    'summary': 'Manage telecom service consumptions',
    'author': 'Community',
    'website': '',
    'depends': [
        'base',
        'product',
        'base_rest',
    ],
    'data': [
        'security/ir.model.access.csv',
        'data/product_category_demo.xml',
        'views/consumptions_views.xml',
        'views/product_views.xml',
        'wizard/import_wizard_view.xml',
    ],
    'demo': [
        'demo/products_demo.xml',
        'demo/consumptions_demo.xml',
    ],
    'installable': True,
    'application': True,
}
